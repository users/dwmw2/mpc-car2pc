OPTFLAGS := -g -O2 -Wall
CFLAGS += $(shell pkg-config --cflags libmpd) $(OPTFLAGS)
LDFLAGS += $(shell pkg-config --libs libmpd)

all: mpc-car2pc

clean:
	rm -f mpc-car2pc *.o
